const mongoose = require("mongoose");

const addto_cart_schema = new mongoose.Schema({
    items: [
            {
            id: {
                type : String,                
                required: [true, "ProducId is required"]
            },
            name: {
                type : String,
                required: [true, "ProductName is required"]
            },
            quantity: {
                type : Number,
                default: 0,
                required: [true, "Quantity is required"]
            },
            price:{
                type: Number,
                default: 0,
                required: [true, "Price is required"]
            }
            }
    ],
    totalAmount: {
        type: Number,
        default: 0,
        required: [true, "Total Amount is Required"]
    },
    userId : {
        type: String,
        required: [true, "User Id is required"]
    }

    }
)

module.exports = mongoose.model("Add To Cart", addto_cart_schema);
