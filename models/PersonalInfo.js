const mongoose = require("mongoose");

// Task Rosales
const personal_info_schema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true,"User Id is required"]
    },
    firstName : {
        type : String,
        required : [true, "First Name is required"]
    },
    lastName : {
        type : String,
        required : [false, "Last Name is required"]
    },
    street : {
        type: String,
        required: [true, "Street is required"]
    },
    barangay : {
        type: String,
        required: [true, "Barangay is required"]
    },
    city: {
        type: String,
        required: [true, "Town/City is required"]
    },
    province : {
        type: String,
        required: [true, "Province is required"]
    },
    phoneNo : {
        type: String,
        required: [true, "Phone Number is required"]
    }
})

module.exports = mongoose.model("Personal Info", personal_info_schema);

