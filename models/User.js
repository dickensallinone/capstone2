const mongoose = require("mongoose");

// Task Rosales
const user_schema = new mongoose.Schema({
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type: Boolean,
        default: false,
        required: [true, "This field is required"]
    },
    items: [
        {
        id: {
            type : String,                
            required: [true, "ProducId is required"]
        },
        name: {
            type : String,
            required: [true, "ProductName is required"]
        },
        quantity: {
            type : Number,
            default: 0,
            required: [true, "Quantity is required"]
        },
        price:{
            type: Number,
            default: 0,
            required: [true, "Price is required"]
        }
        }
        ],
        totalAmount: {
            type: Number,
            default: 0,
            required: [true, "Total Amount is Required"]
        }
})

module.exports = mongoose.model("User", user_schema);

