const mongoose = require("mongoose");

// Task Rosales
const ordered_products_schema = new mongoose.Schema({
        products : [
            {
            productId: {
                type : String,
                required: [true, "ProducId is required"]
            },
            productName: {
                type : String,
                required: [true, "ProductName is required"]
            },
            quantity: {
                type : Number,
                required: [true, "Quantity is required"]
            }
            }
        ],
        totalAmount : {
            type : Number,
            required: [true, "TotalAmount is required"]
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        },
        userId: {
            type: String,
            required: [true, "User Id is Required"]
        }
    }
)

module.exports = mongoose.model("Ordered Products Schema", ordered_products_schema);
