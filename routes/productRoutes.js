const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/ProductController.js");
const OrderedProductsController = require("../controllers/OrderedProductsController.js")
const auth = require('../auth.js');

router.post("/createproduct", auth.verify, auth.verifyAdmin, (request, response) => {
    ProductController.createProduct(request.body).then(result => {
        response.send(result)
    })
})

router.get("/allproducts", (request, response) => {
    ProductController.getAllProducts(request,response)
})

router.get("/allactiveproducts", (request, response) => {
    ProductController.getAllActiveProducts(request,response)
})

router.get("/:id", (request, response) => {
    ProductController.getProduct(request,response)
})


router.put("/:id/updateproduct",auth.verify, auth.verifyAdmin, (request, response) => {
    ProductController.updateProduct(request,response)
})

router.put("/:id/archiveproduct", auth.verify, auth.verifyAdmin,(request, response) => {
    ProductController.archiveProduct(request,response)
})

router.put("/:id/activateproduct", auth.verify, auth.verifyAdmin,(request, response) => {
    ProductController.activateProduct(request,response)
})

router.get("/list/usersorderedproducts", (req,res)=>{

    OrderedProductsController.getOrderedProducts(req,res);
})

 
    
module.exports = router;