const express = require('express');
const router = express.Router();
const auth = require('../auth');
const UserController = require('../controllers/UserController.js');

router.post('/checkemailexists',(req,res)=>{
    UserController.checkEmailExists(req.body).then(result =>{
        res.send(result)
    })
})

router.post('/register',(req,res)=>{
    UserController.registerUser(req.body).then(result =>{
        res.send(result)
    })
})

router.post('/login',(req,res)=>{
    UserController.loginUser(req,res);
})

router.get('/profile', auth.verify, (req,res) => {
	UserController.getProfile(req,res);
})

router.post('/setuserasadmin',auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setUserAsAdmin(request,response).then((result) => {
		response.send(result);
	}) 
})

router.get('/deliveryinfo', auth.verify, (request, response) => {
	UserController.checkDeliveryInfo(request,response);
})

router.post('/adddeliveryinfo', auth.verify, (request, response) => {
	UserController.addDeliveryInfo(request,response);
})

router.post('/checkout', auth.verify, (request, response) => {
	UserController.checkOut(request,response);
})

router.put('/addtocart',auth.verify, (request,response)=>{
	UserController.addToCart(request,response);
})

router.get('/addtocart',auth.verify,(request,response)=>{
	UserController.getItemsFromCart(request,response);
})

router.get('/retrieveallorders',auth.verify, auth.verifyAdmin, (request,response)=>{
	UserController.retrieveAuthenticatedUsersOrders(request,response)
})

router.get('/retrieveusersorders',auth.verify, (request,response)=>{
	UserController.retrieveUsersOrders(request,response)
})

router.get('/:id/totalprice', auth.verify, (request, response) => {
	UserController.totalPriceOfOrders(request,response)
})

router.get('/totalpurchaseamount',auth.verify,(req,res)=>{
	UserController.totalPurchasedAmount(req,res);
})

router.get("/sample",auth.verify,(req,res)=>{
	UserController.sample(req,res);
})

router.post("/updatecredentials",auth.verify,(req,res)=>{
	UserController.updateLoginCredentials(req,res);
})

router.get("/allusers",	(req,res)=>{
	UserController.getAllUsers(req,res);
})
 
router.get("/someprofile",auth.verify,(req,res)=>{
	UserController.getSomeProfile(req,res);
})



module.exports = router;