const OrderedProducts = require('../models/OrderedProducts');

module.exports.getOrderedProducts = async(req,res)=>{
    
    const order  =await OrderedProducts.aggregate([
        { $unwind: "$products" },
        {$group: {
            _id: "$products.productId",
            productName: {$first: "$products.productName"},
            quantity: {$sum: "$products.quantity"}
        }},
        {
            $project: {
              _id: 0,
              productId: "$_id",
              productName: 1,
              quantity: 1
            }
          }
    ])
    return res.send({
        order:order
    });

}