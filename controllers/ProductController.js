
const Product= require("../models/Product.js");

module.exports.createProduct = (request_body) => {
    let new_product = new Product({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    });

    return new_product.save().then((product, error) => {
        if (error) return false;

        return true
    }).catch(error => console.log(error));
}

module.exports.getAllProducts = (request,response)=>{
    return Product.find({}).then((result)=>{
        return response.send(result)
    })
}

module.exports.getAllActiveProducts = (request,response)=>{
    return Product.find({isActive: true}).then((result)=>{
        return response.send(result)
    })
}

module.exports.getProduct = (request,response)=>{
    return Product.findById(request.params.id).then((result)=>{
        return response.send(result)
    })
}

module.exports.updateProduct = (request,response)=>{
   let updated_product_details = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price
   }

   return Product.findByIdAndUpdate(request.params.id,updated_product_details)
   .then((course,error)=>{
    if(error){
        return response.send({
            message: error.message
        })
    }

    return response.send({
        message: "Product has been updated successfully"
    })
   })
}

module.exports.archiveProduct = (request,response)=>{
    return Product.findById(request.params.id).then(product=> {
        if(product.isActive === false){
            return response.send('Cannot Archive Product if it is already deactivated')
        }

        product.isActive = false;
        product.save().then((result,error)=>{
            if(error) return response.send(false)
            return response.send(true)
        }).catch(error => response.send(error.message))
    })
}

module.exports.activateProduct = (request,response)=>{
    return Product.findById(request.params.id).then(product=> {
        if(product.isActive === true){
            return response.send('Product is already activated')
        }

        product.isActive = true;
        product.save().then((result,error)=>{
            if(error) return response.send(false)
            return response.send(true)
        }).catch(error => response.send(error.message))
    })
}


