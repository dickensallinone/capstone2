const bcrypt = require('bcrypt');
const auth = require('../auth');
const User = require('../models/User');
const Product = require('../models/Product');
const ProfileInfo = require('../models/PersonalInfo');
const OrderedProducts = require('../models/OrderedProducts');
const PersonalInfo = require('../models/PersonalInfo');
const mongoose= require('mongoose');

module.exports.checkEmailExists = (req_body) =>{
    return User.find({email: req_body.email}).then(user=> {
        if(user.length >0) return true;
        return false
    })
}

module.exports.registerUser = (request_body) =>{
    let new_user = new User({
        email: request_body.email,
        password: bcrypt.hashSync(request_body.password,10),
        isAdmin: request_body.isAdmin
    }) 

    return new_user.save().then((register_user,error)=>{
        if(error) {
            return {
                isRegister: false,
                message: error.message
            }
        }
        return {
            isRegister: true,
            message: 'Successfully registered a user!'
        }
    }).catch(error => console.log(error));
}

module.exports.loginUser = (req,res)=>{

    return User.findOne({email: req.body.email}).then(result =>{
        if(result === null) {
            return res.send({
                isValid: false,
                message: 'Error with the credentials, please try again'
            });
        }

        else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
            if(isPasswordCorrect) {
                return res.send({
                    isValid: true,
                    accessToken: auth.createAccessToken(result)
                });
            }else {
                return res.send({
                    isValid: false,
                    message: 'Please check your username and password combination'
                })
            }
        }
        
    }).catch(error => response.send(error))
}

module.exports.getProfile =async (req,res) => {
    const userIdPk = new mongoose.Types.ObjectId(req.user.id);
    
    return await User.aggregate(
      [{
        $match: {_id: userIdPk}
      },
        {
        $project: {
            _id: 1,
            email: 1,
            isAdmin: 1
        }
      },
        {
            $lookup: {
                from: "personal infos",
                let: { userId: { $toString: "$_id" } }, 
                pipeline: [
                    { $match: { $expr: { $eq: ["$$userId", { $toString: "$userId" }] } } }, 
                    {$project: {userId: 0,_id:0}}
                ],
                as: "details"
            }
        }
      ]
    ).then((result,error)=>{
        if(error) {
            res.send(error.message)
        }

        res.send(result)
    })
}


// 

module.exports.setUserAsAdmin = async (request,response)=>{

    if(!request.user.isAdmin){
        return response.send('Action Forbidden');
    }

    await User.findById(request.body.userId).
    then(user =>{
        if(user.isAdmin) return response.send('User is already an Admin')
        
        user.isAdmin = true;

        return user.save()
        .then(updatedUser => response.send('User has been updated from regular User to an Admin'))
        .catch(error => response.send(error.message))
    })

   
}

module.exports.checkOut = async (request,response) =>{
    if(request.user.isAdmin) {
        return response.send('Action Forbidden');
    }

    let orderedProducts = new OrderedProducts({
        products: request.body.products,
        totalAmount: request.body.totalAmount,
        userId: request.user.id
    })
    
    let isOrderedProductSuccessful = await orderedProducts.save().then((res,err)=>{
        if(err){
            return null;
        }else return res;
    });
   
    if(!isOrderedProductSuccessful || isOrderedProductSuccessful === null) {
        return response.send({
            isSuccessful: false,
            message: 'Something went wrong'
        })
    }

    let isProductOrdersUpdated = await Promise.all(request.body.products.map(
        async (val)=>{
        return Product.findById(val.productId)
        .then(async(resultProduct)=>{
            let newUserOrder = {
                userId: request.user.id,
                orderId: isOrderedProductSuccessful._id
            }
            resultProduct.userOrders.push(newUserOrder);
            return await resultProduct.save().then(result=>true);
        })
        
    }))
    

    const productUpdateTrue = isProductOrdersUpdated.every(val => val === true)
    if(!productUpdateTrue) {
        return response.send({
            isSuccessful: false,
            message: 'Something went wrong'
        })
    }

    if(isOrderedProductSuccessful && productUpdateTrue) {
        return response.send({
            isSuccessful: true,
            message: 'Checkout Successful'
        })
    }

}

module.exports.addToCart = async (request,response)=>{
    if(request.user.isAdmin) {
        return response.send('Not authorized to do this task')
    }

    return User.findById(request.user.id).then(async(resultUser)=>
        { 
        resultUser.items = request.body.items;
        resultUser.totalAmount = request.body.totalAmount;
        return await resultUser.save().then(result =>response.send(true));
    });
}

module.exports.getItemsFromCart = async(request,response)=>{


    return User.findById(request.user.id).then((resultUser,err)=>
        { 
       if(err) {
        response.send(err);
       }

       response.send({
        items: resultUser.items,
        totalAmount: resultUser.totalAmount
       })
    });
}

module.exports.checkDeliveryInfo = async(request,response)=>{
    if(request.user.isAdmin) {
        return response.send('Not authorized to do this task')
    }

   let isDeliveryInfoAvailable = await ProfileInfo.findOne({userId: request.user.id}).then(resultUser=>{
    if(resultUser) {
        return true;
    }else return false;
   })

   return response.send(isDeliveryInfoAvailable);
} 

module.exports.addDeliveryInfo = async(request,response)=>{
    if(request.user.isAdmin) {
        return response.send('Not authorized to do this task')
    }
    let addDeliveryInfo = new PersonalInfo({
        userId: request.user.id,
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        street: request.body.street,
        barangay: request.body.barangay,
        city: request.body.city,
        province: request.body.province,
        phoneNo: request.body.phoneNo        
    })
    
    return addDeliveryInfo.save().then((result,error)=>{

        if(error){
            response.send({
                isSuccessful: false,
                message: "Something went wrong. Please try again!"
            });
        }else {response.send({
            isSuccessful: true,
            message: "Delivery Information has been updated"
        })}
    })
}

module.exports.retrieveAuthenticatedUsersOrders = async(request,response)=>{
    if(!request.user.isAdmin) {
        return response.send('Not authorized to do this task')
    }
    return User.find({isAdmin:false}).then((result)=>{
        return response.send({orders: result.map(val=>val.orderedProduct)});
    })
}

module.exports.retrieveUsersOrders = async(req,res)=>{
    if(req.user.isAdmin) {
        return res.send('Not authorized to do this task')
    }
    const userOrders = await OrderedProducts.find({userId: req.user.id}).then(result=>result);
    return res.send({order:userOrders})

}


module.exports.totalPriceOfOrders = async (request,response)=>{
    if(request.user.isAdmin) {
        return response.send('Not authorized to do this task')
    }
   const totalAmount = await User.findById(request.user.id).then(result=>result);
   const orderIdObject = totalAmount.orderedProduct.find(val => val.id === request.params.id);

   return response.send(orderIdObject.totalAmount);
}

module.exports.totalPurchasedAmount = async (req,res)=>{
    if(req.user.isAdmin){
        return res.send("Not authorized to to this task");
    }
    const allOrderCost = await User.find({_id: req.user.id}).then(result => result.orderedProduct);

    res.send(allOrderCost);
}

module.exports.sample = async(req,res)=>{
    const value = await OrderedProducts.aggregate([{$match: {userId: req.user.id}}]);
    return res.send({
        value: value
    })
}

module.exports.updateLoginCredentials = async(req,res)=>{

    const userInfo = await User.findOneAndUpdate(
        {_id: req.user.id},
        {email:req.body.email,
        password: bcrypt.hashSync(req.body.password,10)},
        {returnOriginal:false})

   userInfo.email;
   userInfo.password;

   res.send("Updated Credentials Successful");
}

module.exports.getAllUsers = async(req,res)=>{
    // if(req.user.isAdmin) {
    //     return res.send("You are not authorized to do this task");
    // }
    const val = await User.aggregate([
        { $match: { isAdmin: false } },
        { $project: { _id: 1 } },
        {$lookup: {
            from: "personal infos",
            let: { userId: { $toString: "$_id" } }, 
            pipeline: [
                { $match: { $expr: { $eq: ["$$userId", { $toString: "$userId" }] } } }, 
                {$project: 
                    {firstName: 1,
                    lastName: 1,
                    _id: 0}}
            ],
            as: "personalInfo"
        }},
        {
            $lookup: {
                from: "ordered products schemas",
                let: { userId: { $toString: "$_id" } }, 
                pipeline: [
                    { $match: { $expr: { $eq: ["$$userId", { $toString: "$userId" }] } } }, 
                    {$project: {userId: 0}}
                ],
                as: "details"
            }
        },
     
    ]);


 
    res.send(val)
}


