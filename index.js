
const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');
require('dotenv').config();
const port = 4000;

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use('/api/users/',userRoutes);
app.use('/api/products/',productRoutes);

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-arboiz.p20uafb.mongodb.net/e-commerce-api-mvp?retryWrites=true&w=majority`,{
    useNewUrlParser:true,
    useUnifiedTopology: true

});

let database = mongoose.connection;

database.on('error',()=>console.log('Connection error:('));
database.once('open',()=>console.log('Connected to MongoDB!'));

app.listen(process.env.PORT || port,()=>{
    console.log(`E-commerce API MVP is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;